from django.apps import AppConfig


class SchoolMainConfig(AppConfig):
    name = 'school_main'
